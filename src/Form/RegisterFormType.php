<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class RegisterFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, [
            'label' => 'Name',
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Name'
            ]
        ]);

        $builder->add('surname', TextType::class, [
            'label' => 'Last name',
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Last name'
            ]
        ]);

        $builder->add('email', EmailType::class, [
            'label' => 'Email',
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Email'
            ]
        ]);

        $builder->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'invalid_message' => 'Password doesn´t match',
            'options' => ['attr' => ['class' => 'password-field']],
            'required' => true,
            'first_options' => [
                'label' => 'Password',
                'attr' => [
                    'class' => 'formDiv',
                    'placeholder' => 'Password'
                ]
            ],
            'second_options' => [
                'label' => 'Confirm Password',
                'attr' => [
                    'class' => 'formDiv',
                    'placeholder' => 'Confirm Password'
                ]
            ]
        ]);



    }
}
