<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class PodcastFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class, [
            'label' => 'Title',
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Title'
            ]
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'Description',
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Write about your audio...'
            ]
        ]);

        $builder->add('audio', FileType::class, [
            'label' => 'Upload Audio',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Upload Audio'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'audio/x-wav',
                        'audio/mpeg',
                        'audio/x-aiff',
                        'audio/ogg',
                        'audio/x-m4a'
                    ],
                    'mimeTypesMessage' => 'wav - mp3 - aiff'
                ])
            ]
        ]);

        $builder->add('image', FileType::class, [
            'label' => 'Image',
            'mapped' => false,
            'required' => false,
            'attr' => [
                'class' => 'formDiv',
                'placeholder' => 'Image'
            ],
            'constraints' => [
                new File([
                    'mimeTypes' => [
                        'image/gif',
                        'image/png',
                        'image/jpeg'
                    ],
                    'mimeTypesMessage' => 'png - jpeg'
                ])
            ]
        ]);

    }
}

